# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=font-iosevka
pkgver=29.2.0
pkgrel=0
pkgdesc="Versatile typeface for code, from code"
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-aile
	$pkgname-etoile
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-Iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-IosevkaAile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-IosevkaEtoile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-IosevkaSlab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-SGr-IosevkaCurly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-SGr-IosevkaCurlySlab-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base=$pkgver-r$pkgrel
		$pkgname-aile=$pkgver-r$pkgrel
		$pkgname-etoile=$pkgver-r$pkgrel
		$pkgname-slab=$pkgver-r$pkgrel
		$pkgname-curly=$pkgver-r$pkgrel
		$pkgname-curly-slab=$pkgver-r$pkgrel
		"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/Iosevka-*.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/iosevka/IosevkaAile-*.ttc
}

etoile() {
	pkgdesc="$pkgdesc (Iosevka Etoile)"
	amove usr/share/fonts/iosevka/IosevkaEtoile-*.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/IosevkaSlab-*.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/SGr-IosevkaCurly-*.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/SGr-IosevkaCurlySlab-*.ttc
}

sha512sums="
1677bda3b752a64183767ef61246e8df65b7f4266291420d70baade416f8686a14073be55a68f8193b78f0ed81e288d31afd7eeb82714455aea6abb5bffd9d85  PkgTTC-Iosevka-29.2.0.zip
9008f4800a31142f22f85f94ff5ccc29581cda4067fba83843279e1f170a51c1bffca85e002384ab65aaafd6ce850e88639880012da0521c2e3ecf6397fbbdae  PkgTTC-IosevkaAile-29.2.0.zip
e63d4c98de1fa4dd9cf7248c3384737738dd3fdb22f1debfe14b7482e3d08d1b59199cf952a8288f389de6d8e1f87b140cc09ae0ad6ca128a41d914cff8a9452  PkgTTC-IosevkaEtoile-29.2.0.zip
dca81e6cc00415c10a05943ed862451fc252767e510e7004e0e81a4866c2da249f88ee70fb117f73b15138f48d03589203c628757df69921588d847287a132b4  PkgTTC-IosevkaSlab-29.2.0.zip
fa1712529ee5d6cde804a1bac14cf15f5cf7d399fdf6225046e89d5f0faf9752b3833cfd7cb037f0e856fad144cbfb10d12e1ee71a96b41f8fc96ac6918b5d51  PkgTTC-SGr-IosevkaCurly-29.2.0.zip
2b74fddb2c6040af25207c90282c34b53248e46bd653c5d322727cd3b72069f8a852d1957b50a9c2f5dda8755f31eec101fb21421d273e8d7691e54c3fcaf21f  PkgTTC-SGr-IosevkaCurlySlab-29.2.0.zip
"
